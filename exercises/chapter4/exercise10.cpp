// exercise10.cpp
// Write a program that plays the game “Rock, Paper, Scissors.” If you are not 
// familiar with the game do some research (e.g., on the web using Google). 
// Research is a common task for programmers. Use a switch-statement to solve 
// this exercise. Also, the machine should give random answers (i.e., select the 
// next rock, paper, or scissors randomly). Real randomness is too hard to 
// provide just now, so just build a vector with a sequence of values to be used 
// as “the next value.” If you build the vector into the program, it will always 
// play the same game, so maybe you should let the user enter some values. Try 
// variations to make it less easy for the user to guess which move the machine 
// will make next.

#include "std_lib_facilities.h"

// takes user and computer moves as parameters.
// returns 1 if user won, 0 if user lost and -1 if draw.
int check_winner(char user_move, char computer_move) {
    switch (user_move) {
        case 'r':
            if (computer_move == 'r')
                return -1;
            else if (computer_move == 'p')
                return 0;
            else
                return 1;
            break;
        case 'p':
            if (computer_move == 'r')
                return 1;
            else if (computer_move == 'p')
                return -1;
            else
                return 0;
            break;
        case 's':
            if (computer_move == 'r')
                return 0;
            else if (computer_move == 'p')
                return 1;
            else
                return -1;
            break;
        default:
            return 0;
            break;
    }
}

int main() {
    int number_moves {0};
    int usrwin_counter {0};
    int compwin_counter {0};
    int temp_result {0};
    bool can_exit{false};
    vector<char> c_moves;
    char temp_move {' '};

    // set the moves that the computer will use
    cout << "Please enter the number of moves the computer will use: ";
    while (!can_exit) {
        cin >> number_moves;
        if (number_moves < 1) {
            cout << "The computer needs at least one move.\n";
        }
        else {
            can_exit = true;
        }
    }

    cout << "Moves: r for rock, p for paper, s for scissors\n";
    
    for (int i = 0; i < number_moves; ++i) {
        can_exit = false;
        while (!can_exit) {
            cout << "Please enter a move: ";
            cin >> temp_move;
            if (temp_move == 'p' || temp_move == 'r' || temp_move == 's') {
                can_exit = true;
                c_moves.push_back(temp_move);
            }
            else {
                cout << "The move must be either, 'r', 'p' or 's'\n";
            }
        }
    }
    
    cout << "Let's begin!\n";
    cout << "------------\n";
    for (int i = 0; i < number_moves; ++i) {
        cout << "Please enter a move: ";
        cin >> temp_move;
        temp_result = check_winner(temp_move, c_moves[i]);
        cout << temp_move << " vs " << c_moves[i] << "\n";
        switch (temp_result) {
            case -1:
                cout << "It's a draw!\n";
                ++usrwin_counter;
                ++compwin_counter;
                break;
            case 0:
                cout << "Computer won.\n";
                ++compwin_counter;
                break;
            case 1:
                cout << "You won!\n";
                ++usrwin_counter;
                break;
            default:
                cout << "Something went wrong.\n";
        }
    }
    cout << "\nScores on the doors...\n";
    cout << "You scored: " << usrwin_counter << "\n";
    cout << "Computer scored: " << compwin_counter << "\n";

    if (usrwin_counter > compwin_counter) 
        cout << "You won! Congratulations!\n";
    else if (usrwin_counter == compwin_counter)
        cout << "It's a draw!\n";
    else
        cout << "You lost. Better luck next time.\n";
}