// exercise8.cpp
// There is an old story that the emperor wanted to thank the inventor of the 
// game of chess and asked the inventor to name his reward. The inventor asked 
// for one grain of rice for the first square, 2 for the second, 4 for the 
// third, and so on, doubling for each of the 64 squares. That may sound modest, 
// but there wasn’t that much rice in the empire! Write a program to calculate 
// how many squares are required to give the inventor at least 1000 grains of 
// rice, at least 1,000,000 grains, and at least 1,000,000,000 grains. You’ll 
// need a loop, of course, and probably an int to keep track of which square you 
// are at, an int to keep the number of grains on the current square, and an int 
// to keep track of the grains on all previous squares. We suggest that you 
// write out the value of all your variables for each iteration of the loop so 
// that you can see what’s going on

#include "std_lib_facilities.h"

int main() {
    bool should_quit {false};
    bool milestones[3] = {false, false, false};
    int rice_counter {0};
    int square_counter = {0};

    while (!should_quit) {
        rice_counter += pow(2, square_counter);
        square_counter++;

        if (rice_counter >= 1000 && !milestones[0]) {
            cout << rice_counter << "\n";
            cout << square_counter << "\n";
            milestones[0] = true;
        }
        else if (rice_counter >= 1000000 && !milestones[1]) {
            cout << rice_counter << "\n";
            cout << square_counter << "\n";
            milestones[1] = true;
        }
        else if (rice_counter >= 1000000000 && !milestones[2]) {
            cout << rice_counter << "\n";
            cout << square_counter << "\n";
            should_quit = true;
            milestones[2] = true;
        }
    }
}