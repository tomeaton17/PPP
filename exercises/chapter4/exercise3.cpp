// exercise3.cpp
// If we define the median of a sequence as "a number so that exactly as many
// elements come before it in the sequence as come after it," fix the program
// in §4.6.3 so that it always prints out a median. Hint: A median need not be
// an element of the sequence.

#include "std_lib_facilities.h"

int main() 
{
	vector<double> values; 		// values
	for (double temp; cin>>temp;) // read into temp
		values.push_back(temp); // put temp into vector

	// compute median temperature:
	sort(values);
	cout << (values[values.size() - 1] - values[0]) / 2.0 << "\n";
}
