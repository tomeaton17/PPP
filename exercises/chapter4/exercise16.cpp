// exercise16.cpp
// In the drill, you wrote a program that, given a series of numbers, found the 
// max and min of that series. The number that appears the most times in a
// sequence is called the mode. Create a program that finds the mode of a set
// of positive integers.

#include "std_lib_facilities.h"

int main()
{
    int input {0};
    vector<int> sequence;
    cout << "Enter a sequence seperated by spaces: ";
    while (cin >> input) {
       sequence.push_back(input); 
    }

    // iterate through array and update frequency vector
    vector<int> frequency;
    
    for (size_t i = 0; i < sequence.size(); ++i) {
        frequency.push_back(0);
    }

    for (int x : sequence) {
        ++frequency[x];     
    }

    // find biggest number in frequency vector
    int largest {0};
    for (int a : frequency) {
        if (a > largest) {
            largest = a;
        }
    }

    cout << largest << "\n";
}
