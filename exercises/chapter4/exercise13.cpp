// exercise13.cpp
// Create a program to find all the prime numbers between 1 and 100. There is a
// classic method for doing this, called the "Sieve of Eratosthenes". If you
// don't know that method, get on the web and look it up. Write your program
// using this method.
//
// Using the pseudocode found here: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes#Pseudocode

#include "std_lib_facilities.h"

int main()
{
    vector<int> sieve;
    int counter {0};

    sieve.push_back(false);
    sieve.push_back(false);

    for (int x = 2; x < 101; ++x) {
        sieve.push_back(true);
    }

    for (int i = 2; i <= sqrt(100); ++i) {
        if(sieve[i]) {
            for (int j = (i*i); j <= 100; j += i) {
                sieve[j] = false;
            }
        }
    }

    for (int z : sieve) {
        if(z) {
            cout << counter << "\n";
        }
        ++counter;
    }
}