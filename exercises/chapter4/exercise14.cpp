// exercise14.cpp
// Modify the program described in the previous exercise to take an input value
// max and then find all the prime numbers from 1 to max.

#include "std_lib_facilities.h"

int main()
{
    vector<int> sieve;

    sieve.push_back(false);
    sieve.push_back(false);

    int max {0};
    cout << "Enter upper limit for prime search: ";
    cin >> max;

    for (int x = 2; x < max + 1; ++x) {
        sieve.push_back(true);
    }

    for (int i = 2; i <= sqrt(max); ++i) {
        if(sieve[i]) {
            for (int j = (i*i); j <= max; j += i) {
                sieve[j] = false;
            }
        }
    }

    int counter {0};

    for (int z : sieve) {
        if(z) {
            cout << counter << "\n";
        }
        ++counter;
    }
}
