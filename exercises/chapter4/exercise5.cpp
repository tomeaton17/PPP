// exercise5.cpp
// Write a program that performs as a very simple calculator. Your calculator 
// should be able to handle the four basic math operations — add, subtract,
// multiply, and divide — on two input values. Your program should prompt the 
// user to enter three arguments: two double values and a character to represent 
// an operation. If the entry arguments are 35.6, 24.1, and '+', the program 
// output should be The sum of 35.6 and 24.1 is 59.7. In Chapter 6 we look at a 
// much more sophisticated simple calculator.

#include "std_lib_facilities.h"

int main() {
    double operand_a {0};
    double operand_b {0};
    char operator_a {' '};

    cout << "Welcome to the calculator. Please enter 2 numbers followed by the operation: ";

    while (cin >> operand_a >> operand_b >> operator_a) {
        switch (operator_a) {
            case '+':
                cout << "The sum of " << operand_a << " and " 
                     << operand_b <<  " is " << operand_a + operand_b << "\n";
                break;
            case '-':
                cout << "The difference of " << operand_a << " and " 
                     << operand_b << " is " << operand_a - operand_b << "\n";
                break;
            case '*':
                cout << "The product of " << operand_a << " and " 
                     << operand_b << " is " << operand_a * operand_b << "\n";
                break;
            case '/':
                cout << "The ratio of " << operand_a << " and " 
                     << operand_b << " is " << operand_a / operand_b << "\n";
                break;
            default:
                cout << "I don't know that operator.\n";
                break;
        }
        cout << "Welcome to the calculator. Please enter 2 numbers followed by the operation: ";
    }
    return 0;
}