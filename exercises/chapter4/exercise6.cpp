// exercise6.cpp
// . Make a vector holding the ten string values "zero", "one", . . . "nine". 
// Use that in a program that converts a digit to its corresponding spelled-out 
// value; e.g., the input 7 gives the output seven. Have the same program, using 
// the same input loop, convert spelled-out numbers into their digit form; e.g., 
// the input seven gives the output 7.

#include "std_lib_facilities.h"

const vector<string> names {"zero", "one", "two", "three", "four", "five",
                                "six", "seven", "eight", "nine"};

int get_number(string name) {
    int result {-1};
    for (size_t i; i < names.size(); i++) {
        if (names[i] == name) {
            result = i;
        }
    }
    return result;
}

int main() {
    int number {0};
    string name {" "};

    bool shouldQuit {false};
    
    while (!shouldQuit) {
        cout << "Enter a digit to get it in word form. Enter a number (spelled) to get the digit. Exit to quit: ";
        if (cin >> number) {
            if (number < 10 && number > -1) {
                cout << names[number] << "\n";
            }
            else {
                cout << "Please enter a one digit number.\n";
            }
        }
        else {
            cin.clear(); // Even though we don't know what this is, it is required. More information http://www.stroustrup.com/Programming/Solutions/Ch4/e4-7.cpp
            cin >> name;
            if (name == "exit") {
                shouldQuit = true;
            }
            else {
                int get_number_result = get_number(name);
                if (get_number_result == -1) {
                    cout << "I don't know that number.\n";
                    
                }
                else {
                    cout << get_number_result << "\n";
                }
            }
        }
    }
    
}
