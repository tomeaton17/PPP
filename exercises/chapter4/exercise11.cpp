// exercise11.cpp
// Create a program to find all the prime numbers between 1 and 100. One way to 
// do this is to write a function that will check if a number is prime 
// (i.e., see if the number can be divided by a prime number smaller than itself) 
// using a vector of primes in order (so that if the vector is called primes, 
// primes[0]==2, primes[1]==3, primes[2]==5, etc.). Then write a loop that goes 
// from 1 to 100, checks each number to see if it is a prime, and stores each 
// prime found in a vector. Write another loop that lists the primes you found. 
// You might check your result by comparing your vector of prime numbers with 
// primes. Consider 2 the first prime.

#include "std_lib_facilities.h"

vector<int> primes_lookup = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 
                      59, 61, 67, 71, 73, 79, 83, 89, 97};

bool is_prime(int number)
{
    bool is_prime{false};

    for (int i = 0; i < primes_lookup.size(); ++i) {
        if (primes_lookup[i] == number)
            is_prime = true;
    }
    return is_prime;
}

int main() 
{
    vector<int> primes;
    primes.push_back(2);

    for (int i = 3; i < 101; ++i) {
        if (is_prime(i)) {
            primes.push_back(i);
        }
    }

    cout << "Prime numbers between 0 and 100:\n";

    for (int x : primes) 
        cout << x << "\n"; 
}