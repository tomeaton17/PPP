// exercise4.cpp
// Write a program to play a numbers guessing game. The user thinks of number
// between 1 and 100 and your progrsm asks questions to out what the number is
// (e.g, "Is the number you are thinking of less than 50?"). Your program should
// be able to identify the number after asking no more than seven questions. 
// Hint: Use the < and <= operators and the if else construct.

#include "std_lib_facilities.h"

int main() 
{
	char input {' '};
	int guess {50};
	int max {100};
	int min {0};
	int counter {0};

	cout << "Think of a number between 0 and 100.\n";
	cout << "Type y for yes. + if your number is higher and - if your number is lower.\n";
	
	while (input != 'y') {
		cout << "Is it " << guess << "?: ";
		cin >> input;
		if(input == '+') {
			min = (max + min) / 2;
			guess = (max + min) / 2;
		}
		else if (input == '-') {
			max = (max + min) / 2;
			guess = (max + min) / 2;
		}
		counter += 1;
	}
	cout << "Your number was " << guess << ". It took " << counter << " guess(es) to guess your number.";

}
