// exercise7.cpp
// Modify the “mini calculator” from exercise 5 to accept (just) single-digit numbers written as either digits or spelled out.

#include "std_lib_facilities.h"

const vector<string> names {"zero", "one", "two", "three", "four", "five",
                                "six", "seven", "eight", "nine"};

bool shouldQuit {false};

int get_number() {
    int result {-1};
    int number {0};
    string name;

    if (cin >> number) {
            if (number < 10 && number > -1) {
                result = number;
            }
            else {
                cout << "Please enter a one digit number.\n";
            }
        }
    else {
        cin.clear(); // Even though we don't know what this is, it is required. More information http://www.stroustrup.com/Programming/Solutions/Ch4/e4-7.cpp
        cin >> name;
        for (size_t i = 0; i < names.size(); ++i) {
            if (names[i] == name) {
                result = i;
            }
        }
        if (result == -1) {
            cout << "I don't know that number.\n";
        }
    }
    return result;
}

int main() {
    int operand_a {0};
    int operand_b {0};
    char operator_a {' '};

    cout << "Welcome to the calculator. Please enter 2 numbers followed by the operation: ";

    while (!shouldQuit) {
        operand_a = get_number();
        operand_b = get_number();
        cin >> operator_a;
        
        switch (operator_a) {
            case '+':
                cout << "The sum of " << operand_a << " and " 
                     << operand_b <<  " is " << operand_a + operand_b << "\n";
                break;
            case '-':
                cout << "The difference of " << operand_a << " and " 
                     << operand_b << " is " << operand_a - operand_b << "\n";
                break;
            case '*':
                cout << "The product of " << operand_a << " and " 
                     << operand_b << " is " << operand_a * operand_b << "\n";
                break;
            case '/':
                cout << "The ratio of " << operand_a << " and " 
                     << operand_b << " is " << operand_a / operand_b << "\n";
                break;
            default:
                cout << "I don't know that operator.\n";
                break;
        }
        cout << "Welcome to the calculator. Please enter 2 numbers followed by the operation: ";
    }
    return 0;
}