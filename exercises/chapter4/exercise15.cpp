// exercise15.cpp
// Write a program that takes an input value n and then finds the first n primes.
// 
// Not using the Sieve of Erasthones, its not suited to this problem.

#include "std_lib_facilities.h"

bool is_prime(int number, vector<int> primes)
{
    bool is_prime {true};
    size_t counter {0};

    while (is_prime && counter < primes.size()) {
        if (number % primes[counter] == 0) {
            is_prime = false;
        }
        ++counter;
    }
    return is_prime;
}

int main() {
    vector<int> primes;
    primes.push_back(2); // Have to give first prime for is_prime algorithm to work

    int n {0};
    cout << "Enter a number n, to find the first n primes: ";
    cin >> n;

    int i {3};
    int counter {1};
    while (counter < n) {
        if (is_prime(i, primes)) {
            primes.push_back(i);
            ++counter;
        }
        ++i;
    }
    
    //cout << counter << "\n";

    for (int x : primes)
        cout << x << "\n";
}