// exercise9.cpp
// Try to calculate the number of rice grains that the inventor asked for in 
// exercise 8 above. You’ll find that the number is so large that it won’t fit 
// in  an int or a double. Observe what happens when the number gets too large 
// to represent exactly as an int and as a double. What is the largest number of 
// squares for which you can calculate the exact number of grains (using an int)? 
// What is the largest number of squares for which you can calculate the 
// approximate number of grains (using a double)?

#include "std_lib_facilities.h"

int main() {
    int rice_counter_i {0};
    int square_counter {0};
    
    while (rice_counter_i < 2147483647) { // 2147483647 is maximum value of int
        rice_counter_i += pow(2, square_counter);
        square_counter++;
    }
    cout << "Highest you can go using int to store number of grains of rice\n";
    cout << "--------------------------------------------------------------\n";
    cout << "Number of grains of rice: " << rice_counter_i << "\n";
    cout << "Number of squares used: " << square_counter << "\n";

    square_counter = 0;
    double rice_counter_d {0};

    while (square_counter <= 64) { 
        rice_counter_d += pow(2, square_counter);
        square_counter++;
    }
    cout << "Approximation of no. of grains of rice used using double";
    cout << "--------------------------------------------------------------\n";
    cout << "Number of grains of rice: " << rice_counter_d << "\n";
    cout << "Number of squares used: " << square_counter << "\n";
}