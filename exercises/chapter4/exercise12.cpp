// exercise12.cpp
// Modify the program described in the previous exercise to take an input value 
// max and then find all prime numbers from 1 to max.
//
// This task cannot be done using a primes look up table. A new is_prime function
// will be made.

#include "std_lib_facilities.h"

bool is_prime(int number, vector<int> primes)
{
    bool is_prime {true};
    size_t counter {0};

    while (is_prime && counter < primes.size()) {
        if (number % primes[counter] == 0) {
            is_prime = false;
        }
        ++counter;
    }
    return is_prime;
}

int main() {
    vector<int> primes;
    primes.push_back(2); // Have to give first prime for is_prime algorithm to work

    int max {0};
    cout << "Enter the upper bound of the prime number search: ";
    cin >> max;

    for (int i = 3; i <= max; ++i) {
        if (is_prime(i, primes))
            primes.push_back(i);
    }
    
    for (int x : primes)
        cout << x << "\n";
}