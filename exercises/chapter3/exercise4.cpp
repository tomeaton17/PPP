// exercise4.cpp
// Write a program that prompts the user to enter two integer values. Store
// these values in int variables named val1 and val2. Write your program to 
// determine the smaller, larger, sum, difference, product and ratio of these
// values and report them to the user.

#include "std_lib_facilities.h"

int main() 
{
	int val1, val2;
	cout << "Enter first integer: ";
	cin >> val1;
	cout << "Enter second integer: ";
	cin >> val2;
	cout << "----------------------\n";
	if (val1 > val2) 
	{
		cout << val1 << " is the bigger of the two. " << val2 << " is the smaller\n";
	}
	else if (val2 > val1) 
	{
		cout  << val1 << " is the smaller of the two. " << val2 << " is the bigger\n";
	}
	else 
	{
		cout << val1 << " and " << val2 << " are the same.\n";
	}
	cout << "Sum: " << val1 + val2 << "\n";
	cout << "Difference: " << val1 - val2 << "\n";
	cout << "Product: " << val1 * val2 << "\n";
	cout << "Ratio: " << val1 / val2 << ":1\n"; // As val1 and val2 are int division will be floored.

	return 0;
}
