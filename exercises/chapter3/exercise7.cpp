// exercise7.cpp
// Do exercise 6, but with three string values. So, if the user enters the
// values "Steinbeck, Hemingway, Fitzgerald", the output should be
// "Fitzgerald, Hemingway, Steinbeck".

#include "std_lib_facilities.h"

int main() 
{
	string val1 {""};
	string val2 {""};
	string val3 {""};

	cout << "Please enter three space seperated strings: ";
	cin >> val1 >> val2 >> val3;

	if (val1 < val2) 
	{
		if(val2 < val3) 
		{
			cout << val1 << ", " << val2 << ", " << val3 << "\n";
		}
		else 
		{
			cout << val1 << ", " << val3 << ", " << val2 << "\n";
		}
	}
	else if (val1 > val2) 
	{
		if (val1 < val3) 
		{
			cout << val2 << ", " << val1 << ", " << val3 << "\n";
		}
		else 
		{
			if (val3 < val2) 
			{
				cout << val3 << ", " << val2 << ", " << val1 << "\n";
			}
			else 
			{
				cout << val2 << ", " << val3 << ", " << val1 << "\n";
			}
		}
	}
}
