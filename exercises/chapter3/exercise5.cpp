// exercise5.cpp
// Modify the program above to ask the user to enter floating-point values and
// store them in double variables. Compare the outputs of the two programs for
// some inputs of your choice. Are the results the same? Should they be? What's
// the difference?

#include "std_lib_facilities.h"

int main() 
{
	double val1 {0};
	double val2 {1};
	cout << "Enter first floating-point value: ";
	cin >> val1;
	cout << "Enter second floating-point value: ";
	cin >> val2;
	cout << "----------------------\n";
	if (val1 > val2) 
	{
		cout << val1 << " is the bigger of the two. " << val2 << " is the smaller\n";
	}
	else if (val2 > val1) 
	{
		cout  << val1 << " is the smaller of the two. " << val2 << " is the bigger\n";
	}
	else 
	{
		cout << val1 << " and " << val2 << " are the same.\n";
	}
	cout << "Sum: " << val1 + val2 << "\n";
	cout << "Difference: " << val1 - val2 << "\n";
	cout << "Product: " << val1 * val2 << "\n";
	cout << "Ratio: " << val1 / val2 << ":1\n"; // As val1 and val2 are doubles now division will be correct.

	return 0;
}
