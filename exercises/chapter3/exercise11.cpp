// exercise11.cpp
// Write a program that prompts the user to enter some number of pennies 
// (1-cent coins), nickels (5-cent coins), dimes (10-cent coins), quarters 
// (25-cent coins), half dollars (50-cent coins), and one-dollar coins 
// (100-cent coins). Query the user separately for the number of each size coin, 
// e.g., “How many pennies do you have?” Then your program should print out 
// something like this:
// You have 23 pennies.
// You have 17 nickels.
// You have 14 dimes.
// You have 7 quarters.
// You have 3 half dollars.
// The value of all of your coins is 573 cents.

#include "std_lib_facilities.h"

int main() 
{
	cout << "Enter the number of pennies you have: ";
	int pennies {0};
	cin >> pennies;

	cout << "Enter the number of nickels you have: ";
	int nickels {0};
	cin >> nickels;

	cout << "Enter the number of dimes you have: ";
	int dimes {0};
	cin >> dimes;

	cout << "Enter the number of quarters you have: ";
	int quarters {0};
	cin >> quarters;

	cout << "Enter the number of half-dollars you have: ";
	int half_dollars {0};
	cin >> half_dollars;

	cout << "Enter the number of dollar coins you have: ";
	int dollars {0};
	cin >> dollars;

	if (pennies) 
	{
		if (pennies == 1) 
		{
			cout << "You have 1 penny.\n";
		}
		else 
		{
			cout << "You have " << pennies << " pennies.\n";
		}
	}

	if (nickels) 
	{
		if (nickels == 1) 
		{
			cout << "You have 1 nickel.\n";
		}
		else 
		{
			cout << "You have " << nickels << " nickels.\n";
		}
	}

	if (dimes) 
	{
		if (dimes == 1) 
		{
			cout << "You have 1 dime.\n";
		}
		else 
		{
			cout << "You have " << dimes << " dimes.\n";
		}
	}

	if (quarters) 
	{
		if (quarters == 1) 
		{
			cout << "You have 1 quarter.\n";
		}
		else 
		{
			cout << "You have " << quarters << " quarters.\n";
		}
	}
	
	if (half_dollars) 
	{
		if (half_dollars == 1) 
		
			cout << "You have 1 half dollar.\n";
		}
		else 
		{
			cout << "You have " << half_dollars << " half dollars.\n";
		}
	}

	if (dollars) 
	{
		if (dollars == 1) 
		{
			cout << "You have 1 dollar.\n";
		}
		else 
		{
			cout << "You have " << dollars << " dollars.\n";
		}
	}

	double total {0};
	total = (pennies / 100.0) + (nickels / 20.0) + (dimes / 10.0) + (quarters / 4.0) + (half_dollars / 2.0) + dollars;

	cout << "The value of all your coins is $" << total << "\n";

	return 0;
}
