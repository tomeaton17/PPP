// exercise10.cpp
// Write a program that takes an operation followed by two operands and outputs
// the result. For example:
// + 100 3.4
// * 4 5
// Read the operation into a string called operation and use an if-statement 
// to figure out which operation the user wants, for example, if 
// (operation=="+"). Read the operands into variables of type double. Implement 
// this for operations called +, –, *, /, plus, minus, mul, and div with their 
// obvious meanings.

#include "std_lib_facilities.h"

int main() 
{
	string operation {""};
	double val1 {0};
	double val2 {0};
	
	cout << "Enter an operation followed by two operands: ";
	cin >> operation >> val1 >> val2;

	if (operation == "+") 
	{
		cout << val1 + val2 << "\n";
	}
	else if (operation == "-") 
	{
		cout << val1 - val2 << "\n";
	}
	else if (operation == "*") 
	{
		cout << val1 * val2 << "\n";
	}
	else if (operation == "/") 
	{
		cout << val1 / val2 << "\n";
	}

}
