// exercise2.cpp
// 2. Write a program in C++ that converts from miles to kilometers. Your
// program sohuld have a reasonable prompt for the user to enter a number of
// miles. Hint: There are 1.609 kilometers to the mile.

#include "std_lib_facilities.h"

int main() 
{
	cout << "Enter the number of miles to convert to kilometers: ";
	double miles;
	cin >> miles;
	cout << miles << " miles is " << miles * 1.609 << " kilometers.\n";
	return 0;
}
