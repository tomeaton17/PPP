// exercise3.cpp
// 3. Write a program that doesn't do anything, but declares a number of
// variables with legal and illegal names (such as int double = 0;) so that 
// you can see how the compiler reacts.

int main() 
{
	int double; 	// illegal: variable name cannot be a C++ keyword
	double delete;	// illegal: variable name cannot be a C++ keyword
	bool 1isTrue;	// illegal: variable name cannot start with a number

	char _legal; 	// legal: You can start a variable name with an "_"
	int aNumber; 	// legal
}
