// This program outputs the message "Hello, World!" to the monitor
#include "std_lib_facilities.h"

int main() // C++ programs start by executing the function main
{
	cout << "Hello, programming!\n"; // output "Hello, Programming"
	cout << "Here we go!\n"; // output "Here we go!"
	return 0;
}

