# Programming: Principles and Practices using C++ (2nd Edition)
My solutions to the exercises in the book "Programming: Principles and Practices using C++" (2nd Edition) by Bjarne Stroustrup.

Also included are some of the examples shown in the book.

Please use this if you are studying this book, but I cannot be held responsible if you get a bad grade due to a mistake in this repository.
